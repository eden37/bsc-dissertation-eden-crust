import {APP_INITIALIZER, ErrorHandler, NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HomeModuleModule } from "./_modules/home-module.module";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {appInitializer} from "./_tools/app.initializer";
import {AuthenticationService} from "./_services/authentication.service";
import {JwtInterceptor} from "./_tools/jwt.interceptor";
import {ErrorInterceptor} from "./_tools/error.interceptor";
import {MAT_DIALOG_DEFAULT_OPTIONS} from "@angular/material/dialog";
import { StoreModule } from '@ngrx/store';
import {authReducer} from "./_state/authentication/auth.reducer";
import {EffectsModule} from "@ngrx/effects";
import {AuthEffects} from "./_state/authentication/auth.effects";
import { DatePipe } from '@angular/common';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import {NavComponent} from "./_components/_navbar/nav.component";
import {postReducer} from "./_state/posts/posts.reducer";
import {GlobalErrorHandler} from "./_error_handlers/global-error-handler";
import {ProfileModule} from "./_modules/profile.module";
import { NotificationMenuComponent } from './_components/_notifications/notification-menu.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HomeModuleModule,
    ProfileModule,
    HttpClientModule,
    BrowserAnimationsModule,
    StoreModule.forRoot({
      auth:authReducer,
      post:postReducer
    }, {}),
    EffectsModule.forRoot([AuthEffects])
  ],
  providers: [
    [DatePipe],
    {provide: APP_INITIALIZER, useFactory: appInitializer, multi: true, deps: [AuthenticationService] },
    /*{provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},*/
    {provide: ErrorHandler, useClass: GlobalErrorHandler },
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
  // default options for dialogs
    {
      provide: MAT_DIALOG_DEFAULT_OPTIONS,
      useValue: {
        disableClose: true,
        hasBackdrop: true
      }
    },
  ],
  exports: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
