import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Topic} from "../../_models/topic";
import {TopicService} from "../../_services/topic.service";
import {COMMA, ENTER} from "@angular/cdk/keycodes";
import {filter, Observable, of, startWith} from "rxjs";
import {UserService} from "../../_services/user.service";
import {FileDTO} from "../../_models/file";
import {MaxSizeValidator} from './validators/customFileValidator';
import {catchError, map} from "rxjs/operators";
import {HttpErrorResponse} from "@angular/common/http";
import {ErrorHandlingService} from "../../_services/error-handling.service";
import {MatDialog} from "@angular/material/dialog";
import {AlertDialogComponent} from "../../_error_handlers/alert-dialog.component";
import {MatHorizontalStepper, MatStepper} from "@angular/material/stepper";
import {Router} from "@angular/router";
import {User} from "../../_models/user";
import * as AuthActions from "../../_state/authentication/auth.actions";
import {Store} from "@ngrx/store";
import {ApiResponseEntity} from "../_interfaces/apiResponse";
import ApiResponse = ApiResponseEntity.ApiResponse;


interface Gender {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-registration-stepper',
  templateUrl: './registration-stepper.component.html',
  styleUrls: ['./registration-stepper.component.css']
})
export class RegistrationStepperComponent implements OnInit {

  /* Error Handling */
  showValidationError = false;

  addOnBlur = true;
  loading = false;
  selectable = true;

  imageURL: string;

  selectedImageFile: File;

  myFileName = "Select Files";

  readonly separatorKeysCodes: number[] = [ENTER, COMMA];

  allTopics: any[] = [];
  filteredTopics: Observable<string[]>;

  public user: FormGroup;
  hide = true;
  selectedValue: string;

  genders: Gender[] = [
    {value: 'Male'  , viewValue: 'Male'},
    {value: 'Female', viewValue: 'Female'},
    {value: 'Genderqueer'  , viewValue: 'Genderqueer'},
    {value: 'Bigender'  , viewValue: 'Bigender'},
    {value: 'Genderfluid'  , viewValue: 'Genderfluid'},
    {value: 'Agender'  , viewValue: 'Agender'},
    {value: 'NonBinary'  , viewValue: 'Non - Binary'},
    {value: 'Polygender'  , viewValue: 'Polygender'},
  ];

  @ViewChild('UploadFileInput') uploadFileInput: ElementRef;
  @ViewChild('topicInput') topicInput: ElementRef<HTMLInputElement> | undefined = undefined;

  constructor(
    private fb: FormBuilder,
    private topicService: TopicService,
    private userService: UserService,
    private errorHandlingService : ErrorHandlingService,
    private dialog: MatDialog,
    private router: Router,
    private store: Store,
  ) {

  }

  /*get tagControl(): FormControl {
    return this.topicsInput as FormControl;
  }*/

  get userDetails(): FormArray {
    return this.user.get('userDetails') as FormArray;
  }

  ngOnInit(): void {
    this.loading = true;
    this.topicService.getAll()
      .pipe(map((apiResponse : ApiResponse) => {
        const allData : any = apiResponse.data
        allData.forEach(topic=>{
          this.allTopics.push(topic.title)
        })
        this.loading = false;

      }))
      .subscribe()
    /*this.topicService.getAll().pipe().subscribe(topics => {
        this.loading = false;
        const dataIndex: number = Object.keys(topics).indexOf('data');
        const arrayOfData: any = Object.values<Topic>(topics);
        const allTopics = arrayOfData[dataIndex];
        this.allTopics = allTopics.map(topic => topic.title);
      }*/


    this.user = this.fb.group({
      userDetails: this.fb.array([])
    })

    this.userDetails.push(this.fb.group({
      name:     new FormControl('', [Validators.required]),
      lastName: new FormControl('', [Validators.required]),
      username: new FormControl('', [Validators.required]),
      email:    new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.min(5)])
    }))

    this.userDetails.push(this.fb.group({
      dateOfBirth:  new FormControl('', checkDateOfBirth),
      gender:       new FormControl('', Validators.required),

    }))

    this.userDetails.push(this.fb.group({
      about:        new FormControl('')
    }))

    this.userDetails.push(this.fb.group({
      topics:        new FormControl('')
    }))

    this.userDetails.push(this.fb.group({
      name:   new FormControl(''),
      avatar: new FormControl('', MaxSizeValidator(2000000)),
      accepted: new FormControl('', Validators.required)
    }))
    // Repeat the above written code to push next stepper inputs into the array

  }

  get acceptedTerms() {
    return this.userDetails.get([4]).get('accepted').value;
  }

  get passwordInput() {
    return this.userDetails.get([0]).get('password');
  }

  get topicsInput() {
    return this.userDetails.get([3]).get('topics').value;
  }

  get avatarInput() {
    return this.userDetails.get([4]).get('avatar');
  }


  submitForm(): void {
    const userDetails = this.userDetails.value
    this.store.dispatch(AuthActions.registrationRequest({userDetails}));
    /*this.userService.register(this.userDetails.value)
      .pipe(
        catchError((err:HttpErrorResponse) => {
          if(err.status == 409){
            this.openAlertDialogAsync()
            //this.clearAdditionalForms()
            return of (null)
          }
          return of (null)
        }))
      .subscribe(
       (user: User) => {
         console.log(user)
         console.log("Registered succesfully")
         this.router.navigateByUrl("/home")
       })*/
  }

  openAlertDialogAsync() {
    setTimeout(() => {
      this.dialog.open(AlertDialogComponent, {
        data: {
          icon: 'Error',
          message: 'An account with this username password already exists'
        }
      });
    }, 200);
  }


  showPreview(event) {
    this.selectedImageFile = this.avatarInput.value;
    // File Preview
    const reader = new FileReader();
    reader.readAsDataURL(this.selectedImageFile);
    reader.addEventListener(
      "loadend",
      ev => {
        let readableString = reader.result.toString();
        /* Create the avatar object */
        const avatarDTO : FileDTO = new FileDTO({
          base64: reader.result.toString().split(',')[1] ,
          name: this.selectedImageFile.name,
          size: this.selectedImageFile.size,
          lastModified: this.selectedImageFile.lastModified,
          contentType: this.selectedImageFile.type}
        );
        this.userDetails.get([4]).get('avatar').patchValue(avatarDTO);
        this.userDetails.get([4]).updateValueAndValidity();
        /*document.getElementById("post-preview-image");*/
        /*let postPreviewImage = <HTMLImageElement>document.getElementById("post-preview-image");*/
        /*postPreviewImage.src = readableString;*/
      }
    )
    reader.onload = () => {
      this.imageURL = reader.result as string;
    }
  }
}



function checkDateOfBirth(control: AbstractControl): { [key: string]: boolean} | null {
  let forbidden = true;
  if (!control.value) return forbidden ? {'required': true } : null;
  if (control.value) {
    const moment: Date = control.value;
    const yearNow = new Date().getFullYear();
    if (((yearNow - moment.getFullYear()) > 18) && (moment.getFullYear() <= yearNow)) {
      forbidden = false;
    }
  }
  return forbidden ? { 'invalidDOBYear': true } : null;
}

