import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';


import {BehaviorSubject, Observable} from 'rxjs';
import {ApiResponse, AuthenticationService} from "./authentication.service";
import {Post} from "../_models/post";
import {environment} from "../../environments/environment";
import {map} from "rxjs/operators";
import {Comment} from "../_models/comment";

@Injectable({ providedIn: 'root' })
export class PostService {

  private postsSubject: BehaviorSubject<Post>;
  public posts: Observable<Post>;

  constructor(
    private http: HttpClient,
    private authenticationService: AuthenticationService
  ) {
    this.postsSubject = new BehaviorSubject<Post>(null);
    this.posts = this.postsSubject.asObservable();
  }

  addLike(postID : number) : Observable<ApiResponse<Post>> {
    const userId = this.authenticationService.userValue.id
    const options = {
      params : new HttpParams()
        .append('userId', userId)
        .append('postId', postID),
      withCredentials: true
    };
    return this.http.put<Post>(`${environment.apiUrl}/posts/like`, null, options)
  }

  addDislike(postID : number) : Observable<ApiResponse<Post>> {
    const userId = this.authenticationService.userValue.id
    const options = {
      params : new HttpParams()
        .append('userId', userId)
        .append('postId', postID),
      withCredentials: true
    };
    return this.http.put<Post>(`${environment.apiUrl}/posts/dislike`, null, options)
  }

  submitPost(post: Post): Observable<Post> {
    return this.http.post<Post>(`${environment.apiUrl}/posts/upload`, post, {
      withCredentials: true
    })
      .pipe((map((post) => {
        this.postsSubject.next(post);
        return post;
      })));
  }

  deletePost(postID: number) {
    return this.http.delete(`${environment.apiUrl}/posts/${postID}`, {withCredentials: true})
  }

  addComment(postId: number, comment: Comment) {
    const options = {
      headers: new HttpHeaders({
        // 'Access-Control-Allow-Origin': environment.apiUrl,
        // 'Content-Type': 'application/x-www-form-urlencoded',
        Action: 'addComment',
        'Content-Type': 'application/json',
      }),
      withCredentials: true
    };
    return this.http.post<Post>(`${environment.apiUrl}/posts/${postId}`, comment, options)
  }

  deleteComment(postId: number, comment: Comment) {
    const options = {
      headers: new HttpHeaders({
        // 'Access-Control-Allow-Origin': environment.apiUrl,
        // 'Content-Type': 'application/x-www-form-urlencoded',
        Action: 'deleteComment',
        'Content-Type': 'application/json',
      }),
      body: comment,
      withCredentials: true
    };
    return this.http.delete<Post>(`${environment.apiUrl}/posts/${postId}`, options)
  }
  /*submitPost(post: Post, imageFile?: File): Observable<any> {
    let userInfo = this.authenticationService.loadUserFromLocalStorage();
    console.log(userInfo)
    console.log(userInfo.userName)
    const options = {
      withCredentials: true
    };
    const postEntity = new Blob([JSON.stringify(post)], {
      type: 'application/json'
    });
    const body: FormData = new FormData();
    body.append('entity', postEntity);
    body.append('username', userInfo.userName);
    console.log(options)

    if(imageFile){
      console.log("Sending File Request")
      body.append('file', imageFile, imageFile.name);
      return this.http.post<Post>(`${environment.apiUrl}/posts/upload`, body, options)
        .pipe((map((post) => {
          this.postsSubject.next(post);
          return post;
        })));
    }else {
      console.log("Simple Run");
      return this.http.post<Post>(`${environment.apiUrl}/posts/upload`, postEntity, {
        headers: new HttpHeaders({
          'action': 'uploadSimple'
        }),
        withCredentials: true
      })
        .pipe((map((post) => {
          this.postsSubject.next(post);
          return post;
        })));
    }


  }*/

}
