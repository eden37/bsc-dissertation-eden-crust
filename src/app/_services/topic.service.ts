import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Topic} from "../_models/topic";
import {ApiResponseEntity} from "../_components/_interfaces/apiResponse";
import ApiResponse = ApiResponseEntity.ApiResponse;
import {Observable} from "rxjs";

@Injectable({ providedIn: 'root' })
export class TopicService {
  constructor(
    private http: HttpClient,
  ) {
  }

  findTopic(title: string) {
    const options = {
      headers : new HttpHeaders({
        'action': 'findByTitle',
        'Content-Type': 'application/json',
      }),
      params : new HttpParams()
        .append('title', title),
      withCredentials: true
    }
    return this.http.get<Topic>(`${environment.apiUrl}/topics`, options)
  }

  getAll() : Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${environment.apiUrl}/topics`, {withCredentials: true});
  }

}
