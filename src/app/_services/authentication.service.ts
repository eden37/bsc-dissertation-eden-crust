﻿import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient, HttpContext, HttpHeaders, HttpParams} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {User} from "../_models/user";
import {environment} from "../../environments/environment";
import {FormArray} from "@angular/forms";
import {FileDTO} from "../_models/file";
import {Topic} from "../_models/topic";
import {BYPASS_ERROR} from "../_tools/error.interceptor";

export interface ApiResponse<T> {
  transactionId: string;
  createdAt: string;
  data: T;
  apiError: string;
}


@Injectable({ providedIn: 'root' })
export class AuthenticationService {

    constructor(
        private router: Router,
        private http: HttpClient
    ) {
        this.userSubject = new BehaviorSubject<User>(null);
        this.user = this.userSubject.asObservable();
    }

    public get userValue(): User {
        return this.userSubject.value;
    }
    private userSubject: BehaviorSubject<User>;
    public user: Observable<User>;

    // helper methods

    private refreshTokenTimeout;
    private logoutTokenTimeout;

    login(username: string, password: string) : Observable<any> {
      const options = {
        headers : new HttpHeaders({
          'Content-Type': 'application/json',
          'Content-Length': (username.length + password.length).toString()
        }),
        params : new HttpParams()
          .append('username', username)
          .append('password', password),
        context: new HttpContext().set(BYPASS_ERROR, true),
        withCredentials: true
      };
      return this.http.post<any>(`${environment.apiUrl}/login`, null, options)
        .pipe(map(user => {
          this.userSubject.next(user);
          this.startRefreshTokenTimer();
          return user;
        }))
    }

    register(userDetails: FormArray): Observable<User> {

      /* First Field Values */
      const firstName:  string = userDetails[0]['name'];
      const lastName:   string = userDetails[0]['lastName'];
      const userName:   string = userDetails[0]['username'];
      const password:   string = userDetails[0]['password'];
      const email:      string = userDetails[0]['email'];

      /* Second Field Values */
      const dateOfBirth:  string = userDetails[1]['dateOfBirth'].toISOString().split('T')[0];
      const gender:       string = userDetails[1]['gender'];

      const about:        string = userDetails[2]['about'];

      //if(userDetails[4]['avatar'])
      //const file : FileDTO = userDetails[4]['avatar'];

      const file : FileDTO = !!userDetails[4]['avatar'] ? userDetails[4]['avatar'] : null

      const topics :      Topic[] = [];

      //console.log(file._size);
      if(userDetails[3]['topics']!=''){
        userDetails[3]['topics'].forEach(topic=> topics.push(new Topic({title: topic})))
      }

      //console.log(userDetails[1]['dateOfBirth'].value);

      const user : User = new User({
        firstName: firstName,
        lastName: lastName,
        userName: userName,
        password: password,
        email: email,
        dateOfBirth: dateOfBirth,
        about: about,
        gender: gender,
        avatar: file,
        topics: topics
      })
      return this.http.post<ApiResponse<User>>(`${environment.apiUrl}/auth/register`, user, {withCredentials: true})
        .pipe(
          map((apiResponse: ApiResponse<User>) => {
            this.userSubject.next(apiResponse.data)
            this.startRefreshTokenTimer()
            return apiResponse.data
          })
        )
    }


    logout(): void {
          const options = {
            headers : new HttpHeaders({
              'Content-Type': 'application/json',
            }),
            withCredentials: true};
          this.http.get<any>(`${environment.apiUrl}/auth/token/revoke`, options).subscribe();
          this.stopRefreshTokenTimer();
          localStorage.clear();
          this.userSubject.next(null);
          this.router.navigate(['/login']);
      }

    refreshToken(): Observable<any> {
        //const refreshToken = (document.cookie.split(';').find(x => x.includes('refresh-token')) || '=').split('=')[1];
        const options = {
          headers : new HttpHeaders({
            // 'Access-Control-Allow-Origin': environment.apiUrl,
            // 'Content-Type': 'application/x-www-form-urlencoded',
            //Authorization : `Bearer ${refreshToken}`,
            'Content-Type': 'application/json',
          }),
          withCredentials: true};
        return this.http.get<any>(`${environment.apiUrl}/auth/token/refresh/`,  options)
            .pipe(map((user) => {
                this.userSubject.next(user);
                this.startRefreshTokenTimer();
                this.startLogoutTokenTimer()
                return user;
            }));
    }

    private startLogoutTokenTimer(): void {
      const expires = new Date(this.userValue.accessTokenExpiration);
      const timeout = expires.getTime() - Date.now();
      this.logoutTokenTimeout = setTimeout(() => this.logout(), timeout)
    }

    private startRefreshTokenTimer(): void {
        // set a timeout to refresh the token a minute before it expires
        const expires = new Date(this.userValue.accessTokenExpiration);
        const timeout = expires.getTime() - Date.now() - (60 * 1000);
        this.refreshTokenTimeout = setTimeout(() => this.refreshToken().subscribe(), timeout);
    }

    private stopRefreshTokenTimer(): void {
        clearTimeout(this.refreshTokenTimeout);
    }

    saveUserToLocalStorage(user: User) {
      localStorage.setItem('user-profile', JSON.stringify(user));
    }

    loadUserFromLocalStorage(): User {
      if (this.userValue == null) {
        let fromLocalStorage = localStorage.getItem('user-profile');
        if (fromLocalStorage) {
          let userInfo = JSON.parse(fromLocalStorage);
          this.userSubject.next(userInfo);
        }
      }
      return this.userValue;
    }

    getCookie(name: string): string | null{
      const nameLenPlus = (name.length + 1);
      return document.cookie
        .split(';')
        .map(c => c.trim())
        .filter(cookie => {
          return cookie.substring(0, nameLenPlus) === `${name}=`;
        })
        .map(cookie => {
          return decodeURIComponent(cookie.substring(nameLenPlus));
        })[0] || null;
    }

    /*getErrorMessage()*/
}
