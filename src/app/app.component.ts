import {Component, ViewEncapsulation} from '@angular/core';
import {User} from "./_models/user";
import {AuthenticationService} from "./_services/authentication.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {
  title = 'EdenV2';

  user: User;

  constructor(private authenticationService: AuthenticationService) {
  }

}
