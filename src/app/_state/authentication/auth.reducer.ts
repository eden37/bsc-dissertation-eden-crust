import {User} from "../../_models/user";
import {createFeatureSelector, createReducer, createSelector, on} from "@ngrx/store";
import {loginFailure, loginSuccess, registrationFailure, registrationSuccess} from "./auth.actions";
import {BehaviorSubject, Observable, take} from "rxjs";

export interface State {
  id: number;
  user: User,
  loginError?: string
}

export const initialState: State = {
  id: null,
  user: null,
}

const _authReducer = createReducer(
  initialState,
  on(loginSuccess, registrationSuccess, (state, {user}) => {
    console.log(user.id)
    return {
      ...state,
      user: user,
      id: user.id
    }
  }),
  on(registrationFailure, (state, {message}) => {
    console.log(message);
    console.log(state);
    return {
      ...state,
      loginError: message,
      id:null,
      user:null
    }
  }),
  on(loginFailure, (state, {message}) => {
    console.log(message);
    console.log(state);
    return {
      ...state,
      loginError: message,
      id:null,
      user:null
    }
  })
)

export function authReducer(state, action) {
  return _authReducer(state, action);
}

export const selectAuthState = createFeatureSelector<State>('auth');

export const selectUser = createSelector(
  selectAuthState,
  (state) => state.user
);

export const selectId = createSelector(
  selectAuthState,
  (state) => state.id
);

export const isLoggedIn = createSelector(
  selectAuthState,
  (state) => !!state.id
);
