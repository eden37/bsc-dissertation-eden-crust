import {createAction, props} from "@ngrx/store";
import {User} from "../../_models/user";
import {Observable} from "rxjs";
import {FormArray} from "@angular/forms";

export const loginRequest = createAction(
  '[Auth] Login Request',
  props<{credentials: {username: string, password: string} }>()
);

export const loginSuccess = createAction(
  '[Auth] Login Success',
  props<{user: User}>()
);

export const loginFailure = createAction(
  '[Auth] Login Failure',
  props<{message: string}>()
);

export const registrationRequest = createAction(
  '[Auth] Registration Request',
  props<{userDetails: FormArray}>()
)

export const registrationSuccess = createAction(
  '[Auth] Registration Success',
  props<{user: User}>()
)

export const registrationFailure = createAction(
  '[Auth] Registration Failure',
  props<{message: string}>()
)
