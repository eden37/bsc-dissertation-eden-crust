import {createFeatureSelector, createReducer, createSelector, on} from "@ngrx/store";
import {Post} from "../../_models/post";
import {relatedTopicsFailure, relatedTopicsSuccess} from "./posts.actions";

export interface State {
  posts: Post[]
  errorMessage: string;
}

export const initialState: State = {
  posts: null,
  errorMessage: null
}

const _postReducer = createReducer(
  initialState,
  on(relatedTopicsSuccess, (state, {posts}) => {
    return {
      ...state,
      posts: posts,
      errorMessage: null
    }
  }),
  on(relatedTopicsFailure, (state, {message}) => {
    console.log(message);
    console.log(state);
    return {
      ...state,
      posts: null,
      errorMessage: message
    }
  })
)

export function postReducer(state, action) {
  return _postReducer(state, action);
}

export const selectPostState = createFeatureSelector<State>('auth');

export const selectUser = createSelector(
  selectPostState,
  (state) => state.posts
);

export const selectPosts = createSelector(
  selectPostState,
  (state) => state.posts
);

export const existsPosts = createSelector(
  selectPostState,
  (state) => !!state.posts
);
