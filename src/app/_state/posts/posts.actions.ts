import {createAction, props} from "@ngrx/store";
import {Post} from "../../_models/post";

export const relatedTopicsRequest = createAction(
  '[Post] Related Posts Request',
  props<{options: {limit: number} }>()
);

export const relatedTopicsSuccess = createAction(
  '[Post] Related Posts Success',
  props<{posts: Post[]}>()
);

export const relatedTopicsFailure = createAction(
  '[Post] Related Posts Failure',
  props<{message: string}>()
);
