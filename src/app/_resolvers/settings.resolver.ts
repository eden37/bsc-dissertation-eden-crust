import {User} from "../_models/user";
import {map} from "rxjs/operators";
import {ActivatedRouteSnapshot, Resolve} from "@angular/router";
import {UserService} from "../_services/user.service";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {ApiResponseEntity} from "../_components/_interfaces/apiResponse";
import ApiResponse = ApiResponseEntity.ApiResponse;

@Injectable({
  providedIn: 'root'
})
export class SettingsResolver implements Resolve<any> {

  constructor(private userService: UserService) {
  }

  resolve(route: ActivatedRouteSnapshot): Observable<ApiResponse> {
    return this.userService.getUserTopics()
  }
}
