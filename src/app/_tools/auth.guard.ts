﻿import { Injectable } from '@angular/core';
import {Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree} from '@angular/router';
import {AuthenticationService} from "../_services/authentication.service";
import {first, Observable, tap} from "rxjs";
import * as fromAuth from  '../_state/authentication/auth.reducer'
import {select, Store} from "@ngrx/store";
import * as fromState from "../_state/authentication/auth.reducer";
import {selectUser} from "../_state/authentication/auth.reducer";
import {map} from "rxjs/operators";



@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
    constructor(
      private store$: Store,
      private route: Router,
      private authenticationService: AuthenticationService
    ) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

      return this.checkStoreAuthentication().pipe(
        map((authed) => {
          let userInfo = this.authenticationService.loadUserFromLocalStorage();
          if (!authed && (userInfo==null)) {
            this.route.navigateByUrl('/login');
            console.log(`canActivate( No. Redirect the user back to login. )`);
            return false;
          }
          console.log(`canActivate( Yes. Navigate the user to the requested route. )`);
          return true;
        }),
        first()
      );
  }




  private checkStoreAuthentication(): Observable<boolean> {
    return this.store$.pipe(select(fromState.isLoggedIn)).pipe(first());
  }

      /*        tap(selectUser => {
          if (!selectUser.accessToken) {
            this.router.navigate(['/login'], {queryParams: {returnUrl: state.url}});
            return false;
          }else {

          return true;

          }
        })
      )

    }*/

}
