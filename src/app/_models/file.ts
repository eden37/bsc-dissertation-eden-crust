export class FileDTO {

  base64: string;
  data? : string;
  name?: string;
  size?: number;
  lastModified?: number;
  contentType?: string;

  constructor({
    base64,
    data,
    name,
    size,
    lastModified,
    contentType
  } : {
    base64: string,
    data?: string
    name?: string,
    size?: number,
    lastModified?: number,
    contentType?: string
  }
  ) {
    this.data = (data!=null) ? data : null;
    this.base64 = (base64!=null) ? base64 : null;
    this.name = (name!=null) ? name : null;
    this.size = (size!=null) ? size : null;
    this.lastModified = (lastModified!=null) ? lastModified : null;
    this.contentType = (contentType!=null) ? contentType : null;
  }



}
