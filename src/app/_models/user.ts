﻿import { ResponseEntity } from './base';
import { Topic } from './topic';
import { Post } from './post';
import {FileDTO} from "./file";

export class User extends ResponseEntity{
  id: number;
  userName: string;
  user: string | null;
  password?: string | null;
  firstName: string;
  lastName: string;
  accessToken: string;
  refreshToken: string;
  accessTokenExpiration: number;
  refreshTokenExpiration: number;
  dateOfBirth?: string | null;
  files?: File [] | null;
  avatar?: FileDTO | null | any;
  email?: string | null;
  about?: string | null;
  gender?: string | null;
  topics?: Topic [] | null;
  posts?: Post [] | null;

  constructor(
    {
      firstName,
      lastName,
      userName,
      password,
      email,
      dateOfBirth,
      accessToken,
      refreshToken,
      files,
      avatar,
      about,
      gender,
      topics,
      posts,
      user,
      res_data
    }:
    { firstName? : string,
    lastName? : string,
    userName? : string,
    password? : string,
    email? : string,
    dateOfBirth? : string,
    accessToken? : string,
    refreshToken? : string,
    user? : string,
    files? : File [],
    avatar? : FileDTO,
    about? : string,
    gender? : string,
    topics? : Topic [],
    posts? : Post [],
    res_data? : any }
  ) {
    if(res_data){
      super(res_data);
      this.data = [
        this.id = res_data.id,
        this.userName = res_data.userName,
        this.password = res_data.password,
        this.firstName = res_data.firstName,
        this.lastName = res_data.lastName,
        this.accessToken = res_data.accessToken,
        this.refreshToken = res_data.refreshToken,
        this.dateOfBirth = res_data.dateOfBirth,
        this.user = res_data.user,
        this.email = res_data.email,
        this.about = res_data.about,
        this.gender = res_data.gender,
        this.topics = res_data.topics,
        this.posts = res_data.posts,
      ];
    }else {
      super();
      this.userName = (userName!=null) ? userName : null;
      this.firstName = (firstName!=null) ? firstName : null;
      this.lastName = (lastName!=null) ? lastName : null;
      this.password = (password!=null) ? password : null;
      this.dateOfBirth = (dateOfBirth!=null) ? dateOfBirth : null;
      this.avatar = (avatar!=null) ? avatar : null;
      this.user = (user!=null) ? user : null;
      this.email = (email!=null) ? email : null;
      this.about = (about!=null) ? about : null;
      this.gender = (gender!=null) ? gender : null;
      this.topics = (topics!=null) ? topics : null;
    }

  }

}
