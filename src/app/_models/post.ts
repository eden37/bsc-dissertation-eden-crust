import { ResponseEntity } from "./base";
import { Comment } from "./comment";
import {User} from "./user";
import {Topic} from "./topic";
import {FileDTO} from "./file";

export class Post extends ResponseEntity{
  id: number;
  dateCreated: string;
  dateUpdated: string;
  body: string;
  likes: number;
  topics?: Topic [] | null;
  comments?: Comment [] | null;
  image : FileDTO | null;
  user?: User | null;
  username?: string


  constructor({
    body,
    dateCreated,
    likes,
    user,
    username,
    dateUpdated,
    topics,
    comments,
    image,
    res_data
  } : {
    body?: string,
    dateCreated?: string,
    likes?: number,
    username?: string,
    user?: User,
    dateUpdated?: string,
    topics? : Topic [],
    comments?: Comment [],
    image?: FileDTO,
    res_data? : any
  }
  ) {
    if(res_data){
      super(res_data);
      this.data = [
        this.id = res_data.id,
        this.dateCreated = res_data.dateCreated,
        this.dateUpdated = res_data.dateUpdated,
        this.body = res_data.body,
        this.user = res_data.user,
        this.likes = res_data.likes,
        this.topics = res_data.topics,
        this.comments = res_data.comments,
        this.image = res_data.files
      ];
    }else {
      super();
      this.body = (body != null) ? body : null;
      this.dateCreated = (dateCreated != null) ? dateCreated : null;
      this.dateUpdated = (dateUpdated != null) ? dateUpdated : null;
      this.likes = (likes != null) ? likes : null;
      this.topics = (topics != null) ? topics : null;
      this.comments = (comments != null) ? comments : null;
      this.image = (image != null) ? image : null;
      this.user = (user != null) ? user : null;
      this.username = (username != null) ? username : null;
    }

  }
  //constructor();
}
