import {FileDTO} from "./file";

export interface IPost {
  id: number;
  dateCreated: string;
  dateUpdated: string;
  body: string;
  likes: number;
  topics?: string [] | null;
  comments?: Comment [] | null;
  files? : FileDTO [] | null;
  user?: string;
}
