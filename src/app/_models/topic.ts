import {ResponseEntity} from './base';

export class Topic extends ResponseEntity{
  id: number;
  title: string;
  dateCreated?: string | null;
  dateUpdated?: string | null

  constructor({
    title,
    dateCreated,
    dateUpdated,
    res_data
  } :{
    title?: string,
    dateCreated?: string | null,
    dateUpdated?: string | null,
    res_data? : any
  }
  ) {
    if(res_data){
      super(res_data);
      this.data = [
        this.id = res_data.id,
        this.title = res_data.title,
        this.dateCreated = res_data.dateCreated,
        this.dateUpdated = res_data.dateUpdated
      ]
    }else{
      super();
      this.title = (title!=null) ? title : null;
      this.dateCreated = (dateCreated!=null) ? dateCreated : null;
      this.dateUpdated = (dateUpdated!=null) ? dateUpdated : null;
    }
  }

}
