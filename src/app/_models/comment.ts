import {ResponseEntity} from "./base";
import {User} from "./user";

export class Comment extends ResponseEntity{
  id: number;
  dateCreated: string;
  dateUpdated: string;
  body: string;
  user: User;

  constructor({
    body,
    dateCreated,
    dateUpdated,
    user,
    res_data
  } : {
    body?: string,
    dateCreated?: string,
    dateUpdated? : string,
    user?: User,
    res_data? : any
  }) {
    if(res_data){
      super(res_data);
      this.data = [
        this.id = res_data.id,
        this.dateCreated = res_data.dateCreated,
        this.dateUpdated = res_data.dateUpdated,
        this.user = res_data.user
      ];
    }else{
      super();
      this.body = (body != null) ? body : null;
      this.dateCreated = (dateCreated != null) ? dateCreated : null;
      this.dateUpdated = (dateUpdated != null) ? dateUpdated : null;
      this.user = (user != null) ? user : null;
    }
  }
}
